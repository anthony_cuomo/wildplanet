import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AboutComponent } from './about/about.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { DonateComponent } from './donate/donate.component';
import { SurrenderComponent } from './surrender/surrender.component';
import { VolunteerComponent } from './volunteer/volunteer.component';
import { AdoptComponent } from './adopt/adopt.component';
import { VolFormComponent } from './vol-form/vol-form.component';


@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AppComponent,
    AboutComponent,
    FooterComponent,
    HeaderComponent,
    HomeComponent,
    DonateComponent,
    SurrenderComponent,
    VolunteerComponent,
    AdoptComponent,
    VolFormComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
