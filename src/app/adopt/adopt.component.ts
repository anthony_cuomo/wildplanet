import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adopt',
  templateUrl: './adopt.component.html',
  styleUrls: ['./adopt.component.css']
})
export class AdoptComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  btnClick = function () {
    this.router.navigateByUrl('/home')
  }

}
