import { Component, OnInit } from '@angular/core';
import { Event } from '../event';
import { EVENTS } from '../mock-events';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  events = EVENTS;
  selectedEvent: Event;

  constructor() { }

  ngOnInit() {
  }

  onSelect(event: Event): void {
  this.selectedEvent = event;
}

}
