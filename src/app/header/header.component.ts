import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    title = 'Wild Planet';
    quote = '"For People Who Love Animals"';

  constructor() { }

  ngOnInit() {
  }

}
