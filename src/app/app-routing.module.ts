import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { DonateComponent} from './donate/donate.component';
import { SurrenderComponent} from './surrender/surrender.component';
import { VolunteerComponent} from './volunteer/volunteer.component';
import { AdoptComponent} from './adopt/adopt.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'donate', component: DonateComponent },
  { path: 'surrender', component: SurrenderComponent },
  { path: 'volunteer', component: VolunteerComponent },
  { path: 'adopt', component: AdoptComponent},
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }
