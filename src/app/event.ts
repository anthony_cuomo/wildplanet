export class Event {
  id: number;
  name: string;
  desc: string;
}
