import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-vol-form',
  templateUrl: './vol-form.component.html',
  styleUrls: ['./vol-form.component.css']
})
export class VolFormComponent implements OnInit {

  name = new FormControl('');
  address = new FormControl('');
  age = new FormControl('');
  email = new FormControl('');

  constructor() { }

  ngOnInit() {
  }

}
