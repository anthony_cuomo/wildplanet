import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  mission = '"Our goal is to make Wild Planet the destination for people to come when they want to save or surrender an animal. This makes it possible for animals who are down on their luck to have a fighting chance."'

  constructor() { }

  ngOnInit() {
  }

}
