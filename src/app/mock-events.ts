import { Event } from './event';

export const EVENTS: Event[] = [
  { id: 1, name: 'Adoption Event', desc: 'The adoption event will have all the animals from the local shelters on display for adoption. Everyone is welcome to come enjoy the animals and adopt a new pet. There will be food trucks and carnival rides!' },
  { id: 2, name: 'Surrender Event', desc: 'The Surrender event will allow people who can no longer care for their animals in the best way to come and drop off their animal. This will enable the animal to find a new forever home.' },
  { id: 3, name: 'Volunteer Event', desc: 'This event will be held at wild planet and anyone is welcome to come and volunteer to learn about a new animal that one has never taken care of before. This is an opportunity to become a volunteer permanently for Wild Planet.' },
  { id: 4, name: 'Wild Planet Fair', desc: 'The fair is an annual event that takes place at Wild Planet with food trucks, live music, and carnival rides. This is a large fundraiser to help take the best care of the animals.' },
  { id: 5, name: 'Fish Info Session', desc: 'This info session is held at Wild Planet that allows for a person to come learn about how to take care of fish. Guest speaker Joey Mullen will speak for an hour!' },
  { id: 6, name: 'Reptile Info Session', desc: 'This info session is held at Wild Planet. This allows for people who are nervous about getting a reptile to come and learn how to handle reptiles.' },
  { id: 7, name: 'Bird Info Session', desc: 'This info session is held at Wild Planet. This allows for people who are nervous about getting a bird to come and learn how to handle birds.' },
  { id: 8, name: 'Dog Grooming Event', desc: 'This event is held bi-annually to allow people to get their pets groomed at 50% off. Cutting your dogs hair can help them live a happy healthy life.' },
  { id: 9, name: 'Meet and Greet Event', desc: 'This event will allow people who are fans of Wild Planet to come meet the founders of Wild Planet. This is a way to get more familiar with the Wild Planet staff.' },
  { id: 10, name: 'Animal Giveaway Event', desc: 'This event has many animals that are discounted to help find them forever homes. Some animals can be very expensive and this allows people that can not afford the high prices of an animal.' }
];
